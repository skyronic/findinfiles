from gi.repository import GObject, Gedit, Gtk, Gio, GLib, Gdk
from .searchbackend import SearchBackend
import os
import re



class SearchUIManager:
	backend = None
	def __init__(self, data_path, window):
		self.data_path = data_path+"/findinfiles/"
		self.window = window
		self.ui_file_path = os.path.join(self.data_path, 'findinfiles.ui')

	def get_search_widget(self):
		self.builder = Gtk.Builder()

		if (os.path.isfile(self.ui_file_path)):
			self.builder.add_from_file (self.ui_file_path)

		self.containerBox = self.builder.get_object("containerBox")
		self.startButton = self.builder.get_object("searchStartButton")
		self.searchParamsBox = self.builder.get_object("searchParamsBox")
		self.searchResultsBox = self.builder.get_object("searchResultsBox")
		self.searchResultsLabel = self.builder.get_object("searchResultsLabel")
		self.searchResultsStore = self.builder.get_object("searchResultsStore")
		self.searchResultsTreeView = self.builder.get_object("searchResultsTreeView")
		self.searchingSpinnerContainer = self.builder.get_object("searchingSpinnerContainer")
		self.resultsContainer = self.builder.get_object("resultsContainer")
		self.editQueryButton = self.builder.get_object("editQueryButton")
		self.searchCancelButton = self.builder.get_object("searchCancelButton")


		self.searchPhraseEntry = self.builder.get_object("searchPhraseEntry")
		self.folderChooserButton = self.builder.get_object("folderChooserButton")
		self.matchCaseCheckButton = self.builder.get_object("matchCaseCheckButton")
		self.regexpCheckButton = self.builder.get_object("regexpCheckButton")
		self.wholeWordCheckButton = self.builder.get_object("wholeWordCheckButton")
		self.includedTypesEntry = self.builder.get_object("includedTypesEntry")
		self.excludedTypesEntry= self.builder.get_object("excludedTypesEntry")

		# TODO: Set a default folder more intelligently later.
		fileRoot = self.file_browser_root()
		if fileRoot is None:
			fileRoot = os.path.expanduser("~")

		self.folderChooserButton.set_current_folder(fileRoot)

		# for debugging because I don't like using the mouse to set the folder each time
		self.folderChooserButton.set_current_folder(os.path.expanduser("~/.local/share/gedit/plugins/findinfiles/"))

		# Listen to events
		self.startButton.connect("clicked", self.on_start_clicked)
		self.searchPhraseEntry.connect("activate", self.on_start_clicked)
		self.includedTypesEntry.connect("activate", self.on_start_clicked)
		self.excludedTypesEntry.connect("activate", self.on_start_clicked)
		self.searchPhraseEntry.connect("realize", self.grab_focus)
		self.searchPhraseEntry.connect("key-release-event", self.make_find_button_sensitive)
		self.editQueryButton.connect("clicked", self.hide_results)
		self.searchCancelButton.connect("clicked", self.cancel_search)
		self.window.connect("key-press-event", self.on_spinner_keypress)

		self.search_running = False

		return self.containerBox

	def raw_cancel_search(self):
		if not self.backend == None:
			self.backend.cancel_search()


	def file_browser_root(self):
		bus = self.window.get_message_bus()
		if bus.is_registered('/plugins/filebrowser', 'get_root'):
			msg = bus.send_sync('/plugins/filebrowser', 'get_root')
			if msg:
				browser_root = msg.props.location
				if browser_root and browser_root.is_native():
					return browser_root.get_path()
		return None

	def cancel_search(self, ev):
		if self.search_running == False:
			return
		self.raw_cancel_search()
		self.search_running = False

		# Show the query editor again
		self.hide_results(None)

	def grab_focus(self, ev):
		self.searchPhraseEntry.grab_focus()

	def handle_entry (self):
		# Find the active tab in the gedit window
		active_view = self.window.get_active_view()
		# Get the buffer for that view
		search_phrase = ""
		if active_view is not None:
			active_buffer = active_view.get_buffer()
			if active_buffer.get_has_selection():
				start_mark = active_buffer.get_insert()
				end_mark = active_buffer.get_selection_bound()

				start_iter = active_buffer.get_iter_at_mark(start_mark)
				end_iter = active_buffer.get_iter_at_mark(end_mark)

				search_phrase = active_buffer.get_text(start_iter, end_iter, False)
		
		self.searchPhraseEntry.set_text(search_phrase)

		if self.searchPhraseEntry.get_realized():
			self.searchPhraseEntry.grab_focus()

	def make_find_button_sensitive(self, ev, data):
		if len(self.searchPhraseEntry.get_text()) > 0:
			self.startButton.set_sensitive(True)
		else:
			self.startButton.set_sensitive(False)

	def on_start_clicked(self, p):
		self.searchParamsBox.hide()
		escaped_text = GLib.markup_escape_text(self.searchPhraseEntry.get_text())

		self.searchResultsLabel.set_label("Search Results for: <b>%s</b>" % escaped_text)

		# show the spinner
		self.resultsContainer.hide()
		self.searchingSpinnerContainer.show()


		self.searchResultsBox.show()
		self.search_running = True

		self.backend = SearchBackend()
		self.backend.do_search('foo', self)

	def get_params(self):
		self.searchPhrase = self.searchPhraseEntry.get_text()
		params = {
			"searchPath": self.folderChooserButton.get_current_folder(),
			"searchString": self.searchPhraseEntry.get_text(),
			"wholeWord":self.wholeWordCheckButton.get_active(),
			"caseSensitive":self.matchCaseCheckButton.get_active(),
			"regexp":self.regexpCheckButton.get_active()
		}

		include_text = self.includedTypesEntry.get_text()
		exclude_text = self.excludedTypesEntry.get_text()

		if len(include_text) > 0:
			params['includedTypes'] = include_text

		if len(exclude_text) > 0:
			params['excludedTypes'] = exclude_text

		self.params = params
		return params

	def show_results(self, results, params):
		# show the results
		self.backend = None
		self.search_running = False
		self.searchingSpinnerContainer.hide()
		self.resultsContainer.show()
		self.results = results
		self.fileNames = []

		if len(self.results) == 0:
			escaped_text = GLib.markup_escape_text(self.searchPhraseEntry.get_text())
			self.searchResultsLabel.set_label("No results found for: <b>%s</b>" % escaped_text)
		
		reString = self.searchPhrase
		if not params['regexp']:
			reString = re.escape(self.searchPhrase)
		insensitive_replacer = re.compile(reString, re.IGNORECASE)
		for (fileName, fileInfo) in results.items():
			self.fileNames.append(fileName)
			basename = os.path.basename(fileName)
			searchCount = fileInfo['count']
			matches = "matches"
			if searchCount == 1:
				matches = "match"

			countString = "<span foreground='blue'><b> %d %s</b></span>" % (searchCount, matches)
			fileIter = self.searchResultsStore.append(None, [GLib.markup_escape_text(basename) + countString])
			for match in fileInfo['lines']:
				text = GLib.markup_escape_text(match['text'])

				# Add a highlight to the search
				# First, we need to preserve the case of the match. for this we must capture the original 
				# string in the same case
				match = insensitive_replacer.search(text)
				span = match.span()
				preserved_case_query = text[span[0]:span[1]]
				sanitized_query = GLib.markup_escape_text(preserved_case_query)

				text = insensitive_replacer.sub("<span foreground='red'><b>%s</b></span>" % sanitized_query, text)
				self.searchResultsStore.append(fileIter, [text])

		# Expand all
		self.searchResultsTreeView.expand_all ()
		# Listen for click
		self.searchResultsTreeView.connect("row-activated", self.on_row_activated)

	def hide_results(self, ev):
		self.searchResultsBox.hide()
		self.searchParamsBox.show()

		# Clear the results so it doesn't show up again
		self.searchResultsStore.clear()

	def on_row_activated(self, treeview, path, view_column):
		parts = str(path).split(':')
		fileIndex = 0
		matchIndex = 0

		if len(parts) == 1:
			fileIndex = int(parts[0])
		elif len(parts) == 2:
			fileIndex = int(parts[0])
			matchIndex = int(parts[1])

		fileName = self.fileNames[fileIndex]
		if self.results.get(fileName) == None:
			raise Exception ("Can't find file in results... fatal exception")

		match = self.results[fileName]['lines'][matchIndex]
		target_line_number = match['number'] - 1

		# Try to find the tab if it's already open
		tab_found = False
		all_docs = self.window.get_documents()
		target_tab = None
		for doc in all_docs:
			fileItem = doc.get_location()
			if fileItem == None:
				continue
			if fileItem.get_path () == fileName:
				# The document is already open
				doc.goto_line(target_line_number)
				target_tab = Gedit.Tab.get_from_document(doc)
				tab_found = True
				break

		if tab_found == False:
			fileHandle = Gio.File.new_for_path(fileName)
			target_tab = self.window.create_tab_from_location(fileHandle, None, target_line_number, 0, False, True)
			target_tab.get_document().connect("loaded", self.on_new_tab_loaded, target_tab, target_line_number)
		else:
			self.highlight_word_in_tab(target_tab, target_line_number)

	def on_new_tab_loaded(self, document, data, target_tab,  target_line_number):
		self.highlight_word_in_tab(target_tab, target_line_number)

	def on_spinner_keypress(self, view, event):
		if event.keyval == Gdk.KEY_Escape:
			self.cancel_search(None)

	def highlight_word_in_tab(self, target_tab, target_line_number):
		target_tab.get_view().scroll_to_cursor()
		self.window.set_active_tab(target_tab)
		reString = self.searchPhrase
		if not self.params['regexp']:
			reString = re.escape(self.searchPhrase)
		insensitive_search = re.compile(reString, re.IGNORECASE)
		

		# select the target line number
		view = target_tab.get_view()
		file_buffer = view.get_buffer()

		# Get the iter for the line
		line_start_iter = file_buffer.get_iter_at_line(target_line_number)
		line_end_iter = file_buffer.get_iter_at_line(target_line_number)
		line_end_iter.forward_to_line_end()

		# Get the text of the line
		line_text = file_buffer.get_text(line_start_iter, line_end_iter, True)
		find_index = line_text.lower().find(self.searchPhrase.lower())
		m = insensitive_search.search(line_text.lower())
		if m == None:
			return # ? this should never happen. but you know how computers are...
		span = m.span()

		word_start_iter = file_buffer.get_iter_at_line_offset(target_line_number, span[0])
		word_end_iter = file_buffer.get_iter_at_line_offset(target_line_number, span[1])
		# highlight the selection
		file_buffer.select_range(word_start_iter, word_end_iter)


