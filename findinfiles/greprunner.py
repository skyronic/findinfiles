from subprocess import Popen, PIPE
import re
import threading

class GrepRunner(threading.Thread):
    def __init__(self, params, results_callback):
        threading.Thread.__init__(self)
        self.params = params
        self.results_callback = results_callback
        self.cancel_flag = False

    def run(self):
        params = self.params
        grep_cmd = ["grep"]

        # 
        # OPTIONS!!!
        #

        # Make it recursive
        grep_cmd.append ("-r")

        # Ignore binary
        grep_cmd.append ("-I")

        # Ignore .git
        grep_cmd.append ("--exclude-dir=.git")

        # Don't offer color
        grep_cmd.append ("--color=never")

        # show line number for parsing
        grep_cmd.append ("--line-number")


        # Check for excluded file types
        if not params.get('excludedTypes') == None:
            excludedTypes = params['excludedTypes'].split(',')
            for item in excludedTypes:
                grep_cmd.append("--exclude=%s" % item)

        # Check for included file types
        if not params.get('includedTypes') == None:
            includedTypes = params['includedTypes'].split(',')
            for item in includedTypes:
                grep_cmd.append("--include=%s" % item)

        if not params.get('wholeWord') == None:
            if params.get('wholeWord') == True:
                grep_cmd.append("--word-regexp")

        caseInSensitiveFlag = True
        if not params.get('caseSensitive') == None:
            if params.get('caseSensitive') == True:
                caseInSensitiveFlag = False

        if caseInSensitiveFlag:
            grep_cmd.append("--ignore-case")

        #
        # PATTERN
        #
        # Decide the search string
        searchString = params['searchString']

        if not params['regexp']:
            searchString = re.escape(searchString)

        # Specify the serach phrase
        grep_cmd.append ("--regexp=%s" % searchString)

        # 
        # SOURCE
        #
        grep_cmd.append (params['searchPath'])

        # For debugging
        # print ("Running command: ", grep_cmd)

        self.proc = Popen(grep_cmd, stdout=PIPE, universal_newlines=False)

        # Wait for the process to close
        c_lock = threading.Lock()
        c_lock.acquire()
        cv = threading.Condition(c_lock)
        while self.check_continue() == False:
            cv.wait(0.5)
        c_lock.release()

        if self.cancel_flag == False:
            result = self.proc.stdout.read().decode('utf-8')
            self.results_callback(self.process_results(result))

    def check_continue(self):
        ret =  not (self.proc.poll() is None and self.cancel_flag == False)
        return ret

    def cancel_search(self):
        self.cancel_flag = True

        # Hasta la vista, baby
        self.proc.terminate()

    def process_results(self, raw):
        filebucket = dict()
        lines = raw.splitlines()
        for line in lines:
            parts = line.split(":")
            if len(parts) < 3:
                raise Exception("Grep output seems invalid")
            file_name = parts[0]
            if filebucket.get(file_name) == None:
                filebucket[file_name] = {
                    'count': 0,
                    'lines': []
                }
            filebucket[file_name]['count'] += 1
            filebucket[file_name]['lines'].append({
                'number': int(parts[1]),
                'text': (":".join(parts[2:])).strip()
            })
        return filebucket

