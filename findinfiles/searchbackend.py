import threading
from gi.repository import Gtk, Gdk
from .greprunner import GrepRunner

Gdk.threads_init()

class SearchBackend:
	cancelFlag = False
	def do_search(self, query, search_manager):
		# self.st = SearchThread(search_manager.show_results, search_manager.get_params(), self)
		# self.st.start()
		self.search_manager = search_manager
		self.params = search_manager.get_params()
		self.grep_thread = GrepRunner(search_manager.get_params(), self.result_callback)
		self.grep_thread.start()

	def cancel_search(self):
		self.cancelFlag = True
		self.grep_thread.cancel_search()

	def result_callback(self, results):
		if self.cancelFlag == False:
			Gdk.threads_enter()
			self.search_manager.show_results(results, self.params)
			Gdk.threads_leave()

