from gi.repository import GObject, Gedit, Gtk, Gdk, Gio
from .searchui import SearchUIManager
import random, string

class FindinFilesAppActivatable(GObject.Object, Gedit.AppActivatable):

    app = GObject.property(type=Gedit.App)

    def __init__(self):
        GObject.Object.__init__(self)

    def do_activate(self):
        self.app.add_accelerator("<Control><Shift>F", "win.findinfiles", None)
        self.menu_ext = self.extend_menu("search-section")
        item = Gio.MenuItem.new(_("Fin_d in files..."), "win.findinfiles")
        self.menu_ext.append_menu_item(item)

    def do_deactivate(self):
        self.app.remove_accelerator("win.findinfiles", None)
        self.menu_ext = None



class FindInFilesWindowActivatable(GObject.Object, Gedit.WindowActivatable):
    __gtype_name__ = "FindInFilesWindowActivatable"

    window = GObject.property(type=Gedit.Window)
    search_panel_added = False
    searchWidget = None
    search_manager = None

    def __init__(self):
        GObject.Object.__init__(self)

    def do_activate(self):
        action = Gio.SimpleAction(name="findinfiles")
        action.connect('activate', self.add_search_panel)
        self.window.add_action(action)


    def do_deactivate(self):
        self.window.remove_action("findinfiles")
        if self.search_manager is not None:
            self.search_manager.raw_cancel_search()
        # Hide panel as well
        if self.searchWidget is not None:
            bottom_panel = self.window.get_bottom_panel()
            bottom_panel.remove(self.searchWidget)

    def add_search_panel(self, user_data, foo):
        bottom_panel = self.window.get_bottom_panel()
        bottom_panel.set_property('visible', True)

        if self.search_panel_added == False:
            self.search_manager = SearchUIManager(self.plugin_info.get_data_dir(), self.window)
            self.searchWidget = self.search_manager.get_search_widget()
            bottom_panel.add_titled(self.searchWidget, 'search_panel', "Find in Files") # , Gtk.Image.new_from_icon_name(Gtk.STOCK_FIND, Gtk.IconSize.SMALL_TOOLBAR)
            self.search_panel_added = True

        # Poll for selection and automatically set that as search term
        self.search_manager.handle_entry()




